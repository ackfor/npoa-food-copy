import os
import json
import sqlalchemy

from flask import Flask
from flask import render_template, make_response
from flask import request


from datetime import datetime

from sources.general import Writer
from sources.general import MessageTypes
from sources.general import Routes
from sources.general import General

from db_setup import Base, Order_Form, User

from sqlalchemy import create_engine
from sqlalchemy.orm import Session, query

from jinja2 import Template

from forms import LoginForm, RegisterForm, OrderForm

app = Flask(__name__)
app.config['SECRET_KEY'] = 'ob2-y818ujq_$s+xqat3vcq!(2xs&-8!g4@_x%7h8&v=@&!9%-'

@app.route(Routes.base, methods=['POST', 'GET'])
def main_view():
    """
        Метод, который обрабатывает запросы с основной страницы.

        :method POST: Проверяет считает ли страница пользователя зарегистрировавшимся.
            Если да, то он отправляет ему список всех заказов, с кнопкой изменить, около его заказов.
        :method GET: Просто отправляет страницу, со списком равным None
    """
    login_form = LoginForm()
    register_form = RegisterForm()

    Writer().my_print(
        msg='login sucsessfully',
        msg_type=MessageTypes.info)

    if (request.method == "POST"):
        curent_user = request.form.get('curent_user')

        if(curent_user != 'error' and curent_user != None):
            engine = create_engine(General.db_name, echo=True)
            Base.metadata.bind = engine
            session = Session(bind = engine)

            orders_today = session.query(Order_Form).filter(
                Order_Form.datetime.like('____%s' % (datetime.today().strftime("%d%m%Y")))).all()
        
            orders_today_list = []
            for order in orders_today: 
                orders_today_list.append({
                    'id': order.id,
                    'name': order.name,
                    'order': order.order,
                    'comment': order.comment,
                    'all_price': order.all_price,
                    'sale_price': order.sale_price})

            return render_template(
                'base.html',
                login_form = login_form,
                register_form = register_form,
                order_list = orders_today_list,
                curent_user = curent_user)

    return render_template(
        'base.html',
        login_form = login_form,
        register_form = register_form,
        order_list = None)

@app.route(Routes.do_orders, methods=['GET', 'POST'])
def order_view():
    """
        Метод, который обрабатывает запросы со страницы заполнения заказа.
        
        :method POST: Обрабатывает форму со страницы. И записывает заказ в
            базу данных.
        :method GET: Просто отправляет страницу.
    """
    login_form = LoginForm()
    register_form = RegisterForm()
    order_form = OrderForm()
    message = ''

    Writer().my_print(
        msg='Go to do Orders',
        msg_type=MessageTypes.info)
    
    if request.method == 'POST':
        if order_form.validate_on_submit():
            user_form = Order_Form(
            datetime.today().strftime("%H%M%d%m%Y"), # Сохраняет дату на сервере. "ЧасМинутыДеньМесяцГод"
            request.form.get('name'),
            request.form.get('order'),
            request.form.get('comment'),
            request.form.get('all_price'),
            request.form.get('sale_price'))

            engine = create_engine(General.db_name, echo=True)
            Base.metadata.bind = engine
            session = Session(bind = engine)
            session.add(user_form)
            session.commit()
            message = 'Успешно отправлено'

    return render_template(
        'do_orders.html',
        message=message,
        order_form=order_form,
        login_form = login_form,
        register_form = register_form)#!!!!!!!!!!!!!!!!!!

@app.route('/login', methods = ['POST'])
def login_view():
    """
        Метод, который обрабатывает запросы с всплывающей страницы login.
        
        :method POST: Стандартный log in, если если такой есть,
            то он отправляет ему имя обратно. Если нет, то отправляет специальное сообщение
    """
    login_form = LoginForm()
    
    if not login_form.validate_on_submit():
        return json.dumps({"curent_user": "error",  "msg": "Что-то не так"})
    else:
        engine = create_engine(General.db_name, echo=True)
        Base.metadata.bind = engine
        session = Session(bind = engine)

        user_login = session.query(User).filter(User.name == request.form.get('name')).one()

        if (user_login == None):
            return json.dumps({"curent_user": 'error', "msg": "Логин не тот"})
    
        if user_login.password == request.form.get('password'):
            return json.dumps({"curent_user": request.form.get('name'), "msg": "Залогинился"})
        else:
            return json.dumps({"curent_user": 'error', "msg": "Пароль не тот"})

@app.route('/register', methods = ['POST'])
def register_view():
    """
        Метод, который обрабатывает запросы с всплывающей страницы register.
        
        Не проверяет подтверждение пороля.
        :method POST: Стандартный register, если такого пользователя нет, то отправляет "Зарегался",
            иначе специальные сообщения.
    """
    register_form = RegisterForm()

    if register_form.validate_on_submit():#!!!!!!!!!!!!!!!!!!!!!!!!!!! Отключил валидацию, потому что не панимаю как работает.Она всё ломает
        json.dumps({"curent_user": 'error', "msg": "Что-то ввел не так"})
    else:
        engine = create_engine(General.db_name, echo=True)
        Base.metadata.bind = engine
        session = Session(bind = engine)

        is_exist = session.query(User).filter(User.name == request.form.get('name')).count()

        if (is_exist != 0):
            json.dumps({"curent_user": 'error', "msg": "Такой пользователь уже есть"})
        else:
            new_user = User(
            request.form.get('name'),
            request.form.get('mail'),
            request.form.get('password'))

            session.add(new_user)
            session.commit()

            return json.dumps({"curent_user": request.form.get('name'), "msg": "Зарегался"})
        



if __name__ == '__main__':
    app.run(host='0.0.0.0', port=9090, debug=True)
