const CLASS_LIST = {
    MODAL: 'modal',
    MODAL_ACTIVE: 'modal--active',
    MODAL_HAS_SCROLL: 'modal--has-scroll',
    MODAL_DIALOG_BODY: 'modal__window-body',
    TRIGGER_OPEN: 'js-window-open',
    TRIGGER_CLOSE: 'js-window-close',
    TRIGGER_FORM_SUBMIT: 'js-order-form'
};

// Добавляет обработчик на клик по экрану
document.addEventListener('click', (event) => {
    // open 
    /* Если нажали на элемент с классом js-window-open,
        то открывает окно по ссылке (href) у этого элемента*/
    if(event.target.closest(`.${CLASS_LIST.TRIGGER_OPEN}`)) {
        event.preventDefault();

        const target = event.target.closest(`.${CLASS_LIST.TRIGGER_OPEN}`);
        const modalId = target.getAttribute('href').replace('#', '');
        const modal = document.getElementById(modalId);

        document.body.style.paddingRight = `${getScrollbarWidth()}px`;
        document.body.style.overflow = 'hidden';

        modal.classList.add(CLASS_LIST.MODAL_ACTIVE);
    }

    // close
    /* Если нажали на элемент с классом js-window-close,
        то закрывает активное окно*/
    if(
        event.target.closest(`.${CLASS_LIST.TRIGGER_CLOSE}`) ||
        event.target.classList.contains(CLASS_LIST.MODAL_ACTIVE)
    ) {
        event.preventDefault();

        const modal = event.target.closest(`.${CLASS_LIST.MODAL}`);
        modal.classList.remove(CLASS_LIST.MODAL_ACTIVE);

        modal.addEventListener('transitionend', showScroll);
    }
});

/*Узнает ширину скрола, чтобы его скрыть в модальных окнах*/
const getScrollbarWidth = () => {
    const item = document.createElement('div');

    item.style.position = 'absolute';
    item.style.top = '-9999px';
    item.style.width = '50px';
    item.style.height = '50px';
    item.style.overflow = 'scroll';
    item.style.visibility = 'hidden';

    document.body.appendChild(item);
    const scrollBarWidth = item.offsetWidth - item.clientWidth;
    document.body.removeChild(item);

    return scrollBarWidth;
};

/*Переопределяет поведение кнопок:
    "Отправить" у форм, для отправки Ajax запросов
    Перехода на основную страницу
    Кнопки расчета общей стоимости в списке заказов
*/
$(document).ready(function () {
    if(window.sessionStorage.getItem("curent_user")) {
        $("#js-do_orders").attr("href", "/DoOrders");
        $("#js-do_orders").removeClass(CLASS_LIST.TRIGGER_OPEN);
        $("#name").val(sessionStorage.getItem("curent_user"));
    }

    //"Отправить" у формы
    $("#login-form").submit(function(event) {
        sendAjaxForm("login-form", function(response) {
            const json = jQuery.parseJSON(response)
            if(json.curent_user != "error") {
                $("#login-form").trigger("reset");
                $("#js-do_orders").attr("href", "/DoOrders");
                $("#js-do_orders").removeClass(CLASS_LIST.TRIGGER_OPEN);
                window.sessionStorage.setItem("curent_user", json.curent_user);
                $("#log_msg").html(json.msg);
            }
        });
        event.preventDefault();
    });

    //"Отправить" у формы
    $('#register-form').submit(function(event) {
        event.preventDefault();
        if($('#register-form #password').val() != $('#register-form #confirm_password').val()) {
            $("#reg_msg").html('Пароли разные');
        }
        else {
            sendAjaxForm("register-form", function(response) {
                $("#login-form").trigger("reset");
                const json = jQuery.parseJSON(response)
                $("#reg_msg").html(json.msg);
            });
        }
    });

    //Переход на основную страницу
    $('#js-go_main').click(function(event) {
        const msgform = document.createElement("form");
        msgform.action = '/';
        msgform.name = '';
        msgform.method = 'POST';
    
        product = document.createElement("input");
        product.type = "text";
        product.name = "curent_user";

        const curent_user = sessionStorage.getItem("curent_user");
        product.value = curent_user ? curent_user : "error";
    
        msgform.appendChild(product);
        document.body.appendChild(msgform);
        msgform.submit();
    });

    //Кнопка расчета общей стоимости в списке заказов
    $('#calc-btn').click(function(event) {
        let all_price_calc = 0;
        let sale_price_calc = 0;
        let customers = new Set();
        $.each($('tr#order_line'), function (index, value) {
            customers.add($(value).find('#order_customer').text());
            all_price_calc += Number($(value).find('#all_price_cell').text());
            sale_price_calc += Number($(value).find('#sale_price_cell').text());
        });
        const payer = Math.floor(Math.random() * customers.size);

        let i = 0;
        for (customer of customers) {
            if(i == payer) {
                alert(`Полная стоимость: ${ all_price_calc }, \n
                       Стоимость со скидкой: ${sale_price_calc},\n
                       Денгу скидывать: ${customer}`);
                break;
            }
            i++;
        }
    });
});


/* отправка формы через ajax */
function sendAjaxForm(form_ajax, funcSuccess) {
    var form = $("#" + form_ajax);
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize(),
        success: funcSuccess,
        error: function(error) {
            console.log(error);
        }
    });
}