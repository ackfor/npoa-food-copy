from flask_wtf import FlaskForm

from wtforms import StringField, IntegerField, TextAreaField, PasswordField

from wtforms.validators import DataRequired
from wtforms.validators import Length

from wtforms.widgets import TextArea


class LoginForm(FlaskForm):
    """
        Класс для отображения формы входа на страницу (во всплывающем окне)
    """
    name = StringField(
        'Имя',
        validators=[DataRequired()],
        render_kw={"placeholder": "Имя"})

    password = PasswordField(
        'Пароль',
        validators=[DataRequired()],
        render_kw={"placeholder": "Пароль", "autocomplete": "off"})


class RegisterForm(FlaskForm):
    """
        Класс для отображения формы регистрации на странице (во всплывающем окне)
    """
    name = StringField(
        'Имя',
        validators=[DataRequired()],
        render_kw={"placeholder": "Имя"})

    mail = StringField(
        'Майл',
        validators=[DataRequired(),
        Length(min=4)],
        render_kw={"placeholder": "Электронная почта", "autocomplete": "off"})

    password = PasswordField(
        'Пароль',
        validators=[DataRequired(),
        Length(min=4)],
        render_kw={"placeholder": "Пароль", "autocomplete": "off"})

    confirm_password = PasswordField(
        'Подтверждение',
        validators=[DataRequired(),
        Length(min=4)],
        render_kw={"placeholder": "Подтвердите пароль", "autocomplete": "off"})


class OrderForm(FlaskForm):
    """
        Класс для отображения формы для установки заказа на странице
    """
    name = StringField(
        'Имя',
        validators=[DataRequired()],
        render_kw={"readonly": True})

    order = TextAreaField(
        'Заказ',
        validators=[DataRequired()],
        render_kw={"rows": 5, "cols": 36, "placeholder": "Основной заказ", "autocomplete": "off"})

    all_price = IntegerField(
        'Цена',
        validators=[DataRequired()],
        render_kw={"placeholder": "Цена без скидки", "autocomplete": "off"})

    sale_price = IntegerField(
        'Цена со скидкой',
        validators=[DataRequired()],
        render_kw={"placeholder": "Цена со скидкой", "autocomplete": "off"})

    comment = TextAreaField(
        'Комментарий',
        validators=[DataRequired()],
        render_kw={"rows": 3, "cols": 36, "placeholder": "Комментарий", "autocomplete": "off"})
    