from sources.general import General

from sqlalchemy import create_engine
from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey

from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Order_Form(Base):
    """
    Класс для таблицы Заказов.

    :param datetime: Текущая дата на сервере
    :param name: Имя заказывающего
    :param order: Содержание заказа
    :param comment: Комментарий к заказу
    :param all_price: Цена без скидки
    :param sale_price: Цена со скидкой
    """
    __tablename__ = 'order'
    
    def __init__(self, datetime: str, name: str, order: str, comment: str, all_price: int, sale_price: int):
        self.datetime = datetime
        self.name = name
        self.order = order
        self.comment = comment
        self.all_price = all_price
        self.sale_price = sale_price

    def __repr__(self):
        return "<Order(%s)>" % (self.order)

    id = Column(Integer, primary_key = True)
    datetime = Column(String)
    name = Column(String)
    order = Column(String)
    comment = Column(String)
    all_price = Column(Integer)
    sale_price = Column(Integer)



class User (Base):
    """
    Класс для таблицы Пользователей.
    
    :param name: Имя пользователя
    :param mail: Его почта
    :param password: Его пароль
    """
    __tablename__ = 'user'

    def __init__(self, name: str, mail: str, password: str):
        self.name = name
        self.mail = mail
        self.password = password

    def __repr__(self):
        return "<User(Name: (%s), Mail: (%s), Password: (%s))>\n" % (self.name, self.mail, self.password)

    id = Column(Integer, primary_key = True)
    name = Column(String)
    mail = Column(String)
    password = Column(String)

engine = create_engine(General.db_name, echo=True, connect_args={'check_same_thread': False})
Base.metadata.create_all(engine)